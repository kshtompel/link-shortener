package links

import (
	"errors"
	. "github.com/go-sql-driver/mysql"
	"log"
	"ls/internal/app/storage"
)

const MysqlDuplicateErrorNo = 1062
const SlugLength int = 5

type HandlerCreate struct {
	MaxCreateAttempts int
	Repository        interface {
		Create(url string, hash string, slug string) error
		FindByHash(hash string) (storage.Links, error)
	}
	SlugGenerator interface {
		Generate() string
	}
	HashGenerator interface {
		Generate(link string) string
	}
}

func (handler *HandlerCreate) Create(url string) (string, error) {
	if len(url) == 0 {
		return "", errors.New("invalid url length")
	}

	var err error
	var slug string

	hash := handler.HashGenerator.Generate(url)

	link, err := handler.Repository.FindByHash(hash)

	if err == nil {
		return link.ShortLinkSlug, transformToGeneralError(err)
	}

	slug, err = handler.tryToCreate(url, hash)
	createAttempts := 1

	for handler.shouldKeepTryingToCreate(err, createAttempts) {
		slug, err = handler.tryToCreate(url, hash)
		createAttempts++
	}

	return slug, transformToGeneralError(err)
}

func (handler *HandlerCreate) tryToCreate(url string, hash string) (string, error) {
	slug := handler.SlugGenerator.Generate()

	return slug, handler.Repository.Create(url, hash, slug)
}

func (handler *HandlerCreate) shouldKeepTryingToCreate(err error, createAttempts int) bool {
	if createAttempts >= handler.MaxCreateAttempts || noError(err) {
		return false
	}

	return isMysqlDuplicateError(err)
}

func noError(err error) bool {
	return err == nil
}

func isMysqlDuplicateError(err error) bool {
	me, ok := err.(*MySQLError)

	return ok == true && me.Number == MysqlDuplicateErrorNo
}

func transformToGeneralError(err error) error {
	if err != nil {
		log.Println(err)
		err = errors.New("unable to save record in DB")
	}

	return err
}
