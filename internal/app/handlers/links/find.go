package links

import (
	"errors"
	"log"
)

type HandlerFind struct {
	Repository interface {
		FindBySlug(slug string) (string, error)
	}
}

func (handler *HandlerFind) Find(slug string) (string, error) {
	if len(slug) != SlugLength {
		return "", errors.New("invalid slug length")
	}

	link, err := handler.Repository.FindBySlug(slug)

	if err != nil {
		log.Println(err)
		err = errors.New("unable to find record in DB")
	}

	return link, err
}
