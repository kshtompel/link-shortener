package handlers

import (
	"encoding/json"
	"github.com/valyala/fasthttp"
	"net/http"
)

type Render interface {
	JSON(ctx *fasthttp.RequestCtx, code int, data interface{})
}

type ShortLink struct {
	Slug string `json:"slug"`
}

type LongLink struct {
	Url string `json:"url"`
}

type HttpPing struct {
	Message string `json:"ping"`
}

type HttpError struct {
	Message string `json:"message"`
}

type HttpHandler struct {
	FindLinksHandler interface {
		Find(slug string) (string, error)
	}
	CreateLinksHandler interface {
		Create(url string) (string, error)
	}
}

var (
	strContentType = []byte("Content-Type")
	strApplicationJSON = []byte("application/json")
)

func (handler *HttpHandler) JSON(ctx *fasthttp.RequestCtx, code int, data interface{}) {
	ctx.Response.Header.SetCanonical(strContentType, strApplicationJSON)
	ctx.SetStatusCode(code)

	if err := json.NewEncoder(ctx).Encode(data); err != nil {
		ctx.Error(err.Error(), http.StatusInternalServerError)
	}
}

func (handler *HttpHandler) Ping(ctx *fasthttp.RequestCtx) {
	handler.JSON(ctx, http.StatusOK, &HttpPing{"ok"})
}

func (handler *HttpHandler) HandleSlug(ctx *fasthttp.RequestCtx) {
	slug := ctx.UserValue("slug").(string)

	longLink, err := handler.FindLinksHandler.Find(slug)

	if err != nil {
		handler.JSON(ctx, http.StatusUnprocessableEntity, &HttpError{err.Error()})
	} else {
		ctx.Redirect(longLink, http.StatusMovedPermanently)
	}
}

func (handler *HttpHandler) HandleShortLink(ctx *fasthttp.RequestCtx) {
	longLink := &LongLink{}
	if err := json.Unmarshal(ctx.PostBody(), longLink); err != nil {
		handler.JSON(ctx, http.StatusBadRequest, &HttpError{err.Error()})
	}

	slug, err := handler.CreateLinksHandler.Create(longLink.Url)

	if err != nil {
		handler.JSON(ctx, http.StatusUnprocessableEntity, &HttpError{err.Error()})
	} else {
		handler.JSON(ctx, 201, &ShortLink{slug})
	}
}
