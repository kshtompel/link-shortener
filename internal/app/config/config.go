package config

import (
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

type WebServerConfiguration struct {
	Port int `yaml:"http.port"`
}

type DatabaseConfiguration struct {
	Schema   string `yaml:"database.schema"`
	Host     string `yaml:"database.host"`
	Port     int    `yaml:"database.port"`
	User     string `yaml:"database.user"`
	Password string `yaml:"database.password"`
	DBname   string `yaml:"database.dbname"`
}

type Configuration struct {
	Http     WebServerConfiguration `yaml:",inline"`
	Database DatabaseConfiguration  `yaml:",inline"`
}

func ReadYaml(filename string) Configuration {
	var Config Configuration

	source, err := ioutil.ReadFile(filename)
	if err != nil {
		panic(err)
	}
	err = yaml.Unmarshal(source, &Config)
	if err != nil {
		panic(err)
	}

	return Config
}
