package generator

import (
	"math/rand"
	"time"
)

const Letters = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"

type SlugGenerator struct {
	Length  int
	Charset string
}

func (g *SlugGenerator) Generate() string {
	var seededRand = rand.New(rand.NewSource(time.Now().UnixNano()))
	charset := g.shuffleString(seededRand, g.Charset)

	b := make([]byte, g.Length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}

	return string(b)
}

func (g *SlugGenerator) shuffleString(seededRand *rand.Rand, str string) string {
	runes := []rune(str)
	N := len(runes)
	for i := 0; i < N; i++ {
		// choose index uniformly in [i, N-1]
		r := i + seededRand.Intn(N-i)
		runes[r], runes[i] = runes[i], runes[r]
	}

	return string(runes)
}
