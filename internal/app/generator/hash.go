package generator

import (
	"crypto/md5"
	"encoding/hex"
)

type HashGenerator struct{}

func (g *HashGenerator) Generate(link string) string {
	hasher := md5.New()
	hasher.Write([]byte(link))

	return hex.EncodeToString(hasher.Sum(nil))
}
