package storage

import (
	"fmt"
	"ls/internal/app/config"
)

type MysqlConnection struct {
	Config config.DatabaseConfiguration
}

func (c MysqlConnection) String() string {
	return fmt.Sprintf(
		"%v:%v@%v(%v:%v)/%v?parseTime=true",
		c.Config.User,
		c.Config.Password,
		c.Config.Schema,
		c.Config.Host,
		c.Config.Port,
		c.Config.DBname,
	)
}
