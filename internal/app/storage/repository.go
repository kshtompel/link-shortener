package storage

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"log"
	"time"
)

type Links struct {
	ID            uint `gorm:"primary_key;AUTO_INCREMENT"`
	CreatedAt     time.Time
	ShortLinkSlug string `gorm:"type:varchar(6);unique_index:idx_slug"`
	LongLink      string
	LongLinkHash  string `gorm:"type:varchar(32);unique_index:idx_hash"`
}

type LinksRepository struct {
	Connection MysqlConnection
	db         *gorm.DB
}

func (r *LinksRepository) Connect() {
	var err error

	r.db, err = gorm.Open("mysql", r.Connection.String())

	if err != nil {
		log.Panic(err)
	}
}

func (r *LinksRepository) Close() {
	r.db.Close()
}

func (r *LinksRepository) AutoMigrate() {
	if r.db == nil {
		log.Panic("AutoMigrate can't be executed: no connection to db")
		return
	}

	r.db.AutoMigrate(&Links{})
}

func (r *LinksRepository) FindBySlug(slug string) (string, error) {
	var Link Links

	err := r.db.Where(&Links{ShortLinkSlug: slug}).First(&Link).Error

	return Link.LongLink, err
}

func (r *LinksRepository) FindByHash(hash string) (Links, error) {
	var Link Links

	err := r.db.Where(&Links{LongLinkHash: hash}).First(&Link).Error

	return Link, err
}

func (r *LinksRepository) Create(url string, hash string, slug string) error {
	link := Links{LongLink: url, LongLinkHash: hash, ShortLinkSlug: slug}

	return r.db.Create(&link).Error
}
