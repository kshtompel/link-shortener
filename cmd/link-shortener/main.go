package main

import (
	"github.com/fasthttp/router"
	"github.com/valyala/fasthttp"
	"log"
	"ls/internal/app/config"
	"ls/internal/app/generator"
	"ls/internal/app/handlers"
	"ls/internal/app/handlers/links"
	"ls/internal/app/storage"
	"os"
	"strconv"
)

func main() {
	configuration := config.ReadYaml(os.Args[1])

	repository := storage.LinksRepository{Connection: storage.MysqlConnection{Config: configuration.Database}}
	repository.Connect()
	defer repository.Close()

	repository.AutoMigrate()

	Handler := handlers.HttpHandler{
		FindLinksHandler: &links.HandlerFind{Repository:&repository},
		CreateLinksHandler: &links.HandlerCreate{
			MaxCreateAttempts: links.SlugLength,
			Repository:        &repository,
			SlugGenerator:     &generator.SlugGenerator{Length:5, Charset:generator.Letters},
			HashGenerator:     &generator.HashGenerator{},
		},
	}

	r := router.New()
	r.GET("/ping", Handler.Ping)
	r.POST("/short_link", Handler.HandleShortLink)
	r.GET("/g/:slug", Handler.HandleSlug)

	log.Fatal(fasthttp.ListenAndServe(":" + strconv.Itoa(configuration.Http.Port), r.Handler))
}
