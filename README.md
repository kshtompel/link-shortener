# Link shortener implementation with fasthttp

# buld
cp ./configs/config.yml-dist ./configs/config.yml
cd ./build
docker-compose up --build

# Usage
GET /ping
POST /short_link
'{"url": "https://google.com/"}'
```curl -X POST -d '{"url": "https://google.com/"}' http://127.0.0.1:8005/short_link```


GET /g/:slug

# Scale

To scale this solution we can tune server programmatically
https://github.com/valyala/fasthttp#performance-optimization-tips-for-multi-core-systems

Set slug length to 6 or 7

Place server under balancer and launch as much as needed instances

Tune mysql indices and configuration

Replace relational DB with\or use Memory storage (in memory MySQL storage, redis,) to pre cache link between shortlink->long-link 
