#!/usr/bin/env sh

APP_SRC_DIR="/go/src/ls"
APP_BIN_DIR="/go/bin"
APP_DIR="/srv/ls"
APP="link-shortener"
COMPILED_COMMAND="${APP_BIN_DIR}/${APP}"
START_COMMAND="${APP_DIR}/${APP}"

rm -rf ${START_COMMAND}
#while :; do sleep 2073600; done;
if [[ ! -f ${START_COMMAND} ]]; then
    export GO111MODULE=on
    cd ${APP_SRC_DIR}
    go mod vendor
    go build -mod=vendor -o ${COMPILED_COMMAND} ${APP_SRC_DIR}"/cmd/link-shortener/main.go"
fi

# Remove this line to rebuild on every up
# rm -rf ${START_COMMAND}
ln -s ${COMPILED_COMMAND} ${START_COMMAND}


echo 'start server ...'
${START_COMMAND} ${APP_SRC_DIR}"/configs/config.yml"
